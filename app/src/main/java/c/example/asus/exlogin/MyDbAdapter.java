package c.example.asus.exlogin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class MyDbAdapter {

    myDbHelper myHelper;

    public MyDbAdapter(Context context)
    {
        myHelper = new myDbHelper(context);
    }

    public long insertDataStudents(String name, int id, String email, String pass)
    {
        long state=0;
        SQLiteDatabase db = myHelper.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("name",name);
        contentValues.put("id",id);
        contentValues.put("email",email);
        contentValues.put("pass",pass);

        try{
            state = db.insert("students",null,contentValues);
        }
        catch(Exception e){
        }
        return state;
    }


    public long insertDataTeachers(String name, int id, String email, String pass)
    {
        long state=0;
        SQLiteDatabase db = myHelper.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("name2",name);
        contentValues.put("id2",id);
        contentValues.put("email2",email);
        contentValues.put("pass2",pass);

        try{
            state = db.insert("teachers",null,contentValues);
        }
        catch(Exception e){
        }
        return state;
    }


    public String getData1(int idD) {
        SQLiteDatabase db=myHelper.getWritableDatabase();
        Cursor cursor=db.rawQuery("select * from students where id = '"+idD+"'",null);
        StringBuffer buffer=new StringBuffer();
        while (cursor.moveToNext())
        {
            String name = cursor.getString(cursor.getColumnIndex("name"));
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String email = cursor.getString(cursor.getColumnIndex("email"));
            String pass = cursor.getString(cursor.getColumnIndex("pass"));

            buffer.append(("  Name : " + name+" \n  id : "+id+" \n  email : "+email+ " \n  pass : " + pass + " \n"));
        }
        return buffer.toString();
    }


    public String getData2(int idD) {
        SQLiteDatabase db=myHelper.getWritableDatabase();
        Cursor cursor=db.rawQuery("select * from teachers where id2 = '"+idD+"'",null);
        StringBuffer buffer=new StringBuffer();
        while (cursor.moveToNext())
        {
            String name = cursor.getString(cursor.getColumnIndex("name2"));
            int id = cursor.getInt(cursor.getColumnIndex("id2"));
            String email = cursor.getString(cursor.getColumnIndex("email2"));
            String pass = cursor.getString(cursor.getColumnIndex("pass2"));

            buffer.append(("  Name : " + name+" \n  id : "+id+" \n  email : "+email+ " \n  pass : " + pass + " \n"));
        }
        return buffer.toString();
    }


    public String loginStudents(int id , String pass){
        SQLiteDatabase db=myHelper.getWritableDatabase();
        Cursor cursor=db.rawQuery("select * from students where id = '"+id+"' and pass = '"+pass+"'",null);
        if(cursor.getCount() > 0){
            return "true";
        }else return "false";
    }

    public String loginTeachers(int id , String pass){
        SQLiteDatabase db=myHelper.getWritableDatabase();
        Cursor cursor=db.rawQuery("select * from teachers where id2 = '"+id+"' and pass2 = '"+pass+"'",null);
        if(cursor.getCount() > 0){
            return "true";
        }else return "false";
    }

    public String AdminShow(){
        SQLiteDatabase db = myHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT students.name FROM students" , null);
        StringBuffer buffer=new StringBuffer();
        buffer.append("NAMES: \n");
        while (cursor.moveToNext())
        {
            String name = cursor.getString(cursor.getColumnIndex("name"));
            buffer.append("      "+name+"(S)\n");
        }
        Cursor cursor1 = db.rawQuery("SELECT teachers.name2 FROM teachers" , null);
        while(cursor1.moveToNext())
        {
            String name2 = cursor1.getString(cursor1.getColumnIndex("name2"));
            buffer.append("      "+name2+"(T)\n");
        }
        return buffer.toString();

    }


    static class myDbHelper extends SQLiteOpenHelper {
        private Context context;

        public myDbHelper(Context context) {
            super(context, "ExLogin05", null, 2);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            try {
                String Create_Table1 = "create table students(name varchar(50) NOT NULL, id integer primary key NOT NULL," +
                        " email varchar(50) NOT NULL , pass varchar(20) NOT NULL)";
                sqLiteDatabase.execSQL(Create_Table1);
            } catch (Exception e) {
                Toast.makeText(context, "" + e, Toast.LENGTH_SHORT).show();
            }

            try {
                String Create_Table2 = "create table teachers(name2 varchar(50), id2 integer primary key," +
                        " email2 varchar(50) , pass2 varchar(20))";
                sqLiteDatabase.execSQL(Create_Table2);
            }catch (Exception e){
                Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }


    }
}

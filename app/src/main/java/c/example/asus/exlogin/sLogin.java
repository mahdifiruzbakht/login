package c.example.asus.exlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class sLogin extends AppCompatActivity {

    EditText id;
    EditText pass;
    MyDbAdapter helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_login);

        id = findViewById(R.id.id2);
        pass = findViewById(R.id.pass2);
        helper = new MyDbAdapter(this);
    }

    public void login_accept(View view) {

        try {

            String data = helper.loginStudents(Integer.parseInt(id.getText().toString()), pass.getText().toString());
            if (data.equals("true")) {
                NextPageStudents.id = Integer.parseInt(id.getText().toString());
                Intent intent = new Intent(this, NextPageStudents.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Your user or pass is wrong!", Toast.LENGTH_SHORT).show();
                id.setText("");
                pass.setText("");

            }
        }
        catch (Exception e)
        {

        }
    }
}

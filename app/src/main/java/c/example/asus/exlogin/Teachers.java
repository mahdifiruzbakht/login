package c.example.asus.exlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Teachers extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teachers);
    }
    public void register(View view){
        Intent intent1 = new Intent(this , tRegister.class);
        startActivity(intent1);
    }
    public void login(View view){
        Intent intent2 = new Intent(this , tLogin.class);
        startActivity(intent2);
    }
}

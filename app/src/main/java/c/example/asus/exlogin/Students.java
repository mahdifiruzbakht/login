package c.example.asus.exlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Students extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);
    }
    public void register(View view){
         Intent intent1 = new Intent(this , sRegister.class);
         startActivity(intent1);
    }
    public void login(View view){
        Intent intent2 = new Intent(this , sLogin.class);
        startActivity(intent2);
    }
}

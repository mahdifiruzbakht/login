package c.example.asus.exlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class AdminShow extends AppCompatActivity {

    TextView tv;
    MyDbAdapter helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_show);

        tv = findViewById(R.id.textView111);
        helper = new MyDbAdapter(this);

    }
    public void showLogin(View view){
        String data = helper.AdminShow();
        tv.setText(data);
    }
}

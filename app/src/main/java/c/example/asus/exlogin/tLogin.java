package c.example.asus.exlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class tLogin extends AppCompatActivity {

    EditText id;
    EditText pass;
    MyDbAdapter helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t_login);

        id = findViewById(R.id.id22);
        pass = findViewById(R.id.pass22);
        helper = new MyDbAdapter(this);
    }
    public void login_accept2(View view) {

        try {
            String data = helper.loginTeachers(Integer.parseInt(id.getText().toString()) , pass.getText().toString());
            if(data.equals("true")){
                NextPageTeachers.id2 = Integer.parseInt(id.getText().toString());
                Intent intent = new Intent(this , NextPageTeachers.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Your user or pass is wrong!", Toast.LENGTH_SHORT).show();
                id.setText("");
                pass.setText("");
            }
        }
        catch (Exception e)
        {

        }
    }
}

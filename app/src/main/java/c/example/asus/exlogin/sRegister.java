package c.example.asus.exlogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

public class sRegister extends AppCompatActivity {

    EditText name;
    EditText id;
    EditText email;
    EditText pass;
    boolean check = true;


    MyDbAdapter helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_register);

        name = findViewById(R.id.name);
        id = findViewById(R.id.id);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.pass);


        helper = new MyDbAdapter(this);

    }
    public void register_accept(View view){
        String t1 = name.getText().toString();
        int t2 = Integer.parseInt(id.getText().toString());
        String t3 = email.getText().toString();
        String t4 = pass.getText().toString();

        if (t1.matches(".*[!@#$%^&*()=+*-/].*")) {
            Toast.makeText(this, "Not a valuable name!", Toast.LENGTH_SHORT).show();
            name.setText("");
            name.setText("");
            check = false;
        }
        if (!isValid(email.getText().toString())) {
            Toast.makeText(this, "Email False", Toast.LENGTH_SHORT).show();
            check = false;
            email.setText("");
        }
        if (t1.equals("") || String.valueOf(t2).equals("") || t3.equals("") || t4.equals("")){
            Toast.makeText(this, "please complete the fields!", Toast.LENGTH_SHORT).show();
            check = false;
        }
        if (check == true) {
            long state = helper.insertDataStudents(t1, t2, t3, t4);
            if (state > 0) {
                Toast.makeText(this, "Insertion Successful", Toast.LENGTH_SHORT).show();
                name.setText("");
                id.setText("");
                email.setText("");
                pass.setText("");
            }
            else{

                Toast.makeText(this, "Try again!", Toast.LENGTH_SHORT).show();
                name.setText("");
                id.setText("");
                email.setText("");
                pass.setText("");
            }
        }
        else{
            Toast.makeText(this, "Try again!", Toast.LENGTH_SHORT).show();
            name.setText("");
            id.setText("");
            email.setText("");
            pass.setText("");
        }
    }
    public static boolean isValid(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        return pat.matcher(email).matches();
    }
}

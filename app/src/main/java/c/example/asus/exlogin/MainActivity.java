package c.example.asus.exlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
		
		Toast.makeText(this, "Mahdi Firuzbakht", Toast.LENGTH_SHORT).show();
    }
    public void students(View view){
        Intent intent = new Intent(this , Students.class);
        startActivity(intent);
    }
    public void teachers(View view){
        Intent intent = new Intent(this , Teachers.class);
        startActivity(intent);
    }
    public void admin(View view){
        Intent intent = new Intent(this , AdminLogin.class);
        startActivity(intent);
    }
}

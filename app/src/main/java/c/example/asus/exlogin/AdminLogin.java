package c.example.asus.exlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AdminLogin extends AppCompatActivity {

    EditText id;
    EditText pass;
    MyDbAdapter helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        id = findViewById(R.id.id25);
        pass = findViewById(R.id.pass25);
        helper = new MyDbAdapter(this);
    }
    public void AdminLogin(View view) {

        try {

            if (Integer.parseInt(id.getText().toString()) == 1234 && String.valueOf(pass.getText().toString()).equals("1234")) {
                Intent intent = new Intent(this, AdminShow.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Your user or pass is wrong!", Toast.LENGTH_SHORT).show();
                id.setText("");
                pass.setText("");
            }
        }
        catch (Exception e)
        {
        }
    }
}
